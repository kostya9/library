﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.LibraryContent
{
    public class MaterialCover
    {
        public string Name => Material.Name;
        public FullName AuthorName => Material.AuthorName;
        public DateTime Published => Material.Published;
        public Statistics Statistics => Material.Statistics;

        internal LibraryMaterial Material { get; }

        public MaterialCover(LibraryMaterial material)
        {
            Material = material;
        }
    }
}
