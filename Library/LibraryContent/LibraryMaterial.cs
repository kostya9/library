﻿using System;
using System.Net.Configuration;
using Library.Client;

namespace Library.LibraryContent
{
    public class LibraryMaterial
    {
        public LibraryMaterial(string name, FullName authorName, DateTime published, DateTime registeredInLibrary, MaterialType type, string content)
        {
            if(!Enum.IsDefined(typeof(MaterialType), type))
                throw new ArgumentException(nameof(type));

            Name = name ?? throw new ArgumentException(nameof(name));
            AuthorName = authorName ?? throw new ArgumentException(nameof(authorName));
            Published = published;
            RegisteredInLibrary = registeredInLibrary;
            Type = type;
            Statistics = new Statistics();
            Content = content;
        }

        public string Name { get; }
        public FullName AuthorName { get; }
        public DateTime Published { get; }
        public DateTime RegisteredInLibrary { get; }
        public MaterialType Type { get; }
        public Statistics Statistics { get; }
        public string Content { get; }
    }
}
