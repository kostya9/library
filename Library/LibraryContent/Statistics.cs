﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Library.Client;

namespace Library.LibraryContent
{
    public class Statistics
    {
        public int StudentUsages { get; private set; }
        public int TeacherUsages { get; private set; }
        public int Usages => StudentUsages + TeacherUsages;

        internal void UpdateUsages(LibraryClient client)
        {
            if (client.Type == ClientType.Student)
                StudentUsages++;

            else if (client.Type == ClientType.Teacher)
                TeacherUsages++;
        }
    }
}
