﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Client;

namespace Library.LibraryContent.Abonement
{
    class Abonement : IMaterialContainer
    {
        protected readonly List<MaterialWithExpirationDate> MaterialsList;
        private readonly IExpiryDateCalculationStrategy _strategy;
        public IEnumerable<MaterialWithExpirationDate> Materials => MaterialsList.ToArray();

        public Abonement(IExpiryDateCalculationStrategy strategy)
        {
            MaterialsList = new List<MaterialWithExpirationDate>();
            _strategy = strategy ?? throw new ArgumentException(nameof(strategy));
        }


        public virtual bool Add(LibraryMaterial adding)
        {
            if(adding.Type != MaterialType.Physical)
                throw new ArgumentException(nameof(adding));

            if (MaterialsList.Any(materialInUse => materialInUse.Expiry > DateTime.Now))
                return false;

            MaterialsList.Add(new MaterialWithExpirationDate(adding, _strategy.CalculateExpiryDate(adding, DateTime.Now)));

            return true;
        }

        public virtual void Remove(LibraryMaterial material)
        {
            var foundMaterial = MaterialsList.Where(i => i.Material == material).FirstOrDefault();

            if (foundMaterial == null)
                throw new ArgumentException("The book is not in the abonement");

            MaterialsList.Remove(foundMaterial);
        }
    }
}
