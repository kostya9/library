﻿using System;

namespace Library.LibraryContent.Abonement
{
    public interface IExpiryDateCalculationStrategy
    {
        DateTime CalculateExpiryDate(LibraryMaterial material, DateTime from);
    }
}
