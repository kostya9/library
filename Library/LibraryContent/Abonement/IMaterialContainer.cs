﻿using System;
using System.Collections.Generic;

namespace Library.LibraryContent.Abonement
{
    public interface IMaterialContainer
    {
        IEnumerable<MaterialWithExpirationDate> Materials { get; }
    }
}
