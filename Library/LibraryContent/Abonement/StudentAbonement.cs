﻿using Library.Client;

namespace Library.LibraryContent.Abonement
{
    class StudentAbonement : Abonement
    {
        public StudentAbonement(IExpiryDateCalculationStrategy strategy) : base(strategy)
        {
        }

        public override bool Add(LibraryMaterial book)
        {
            if (MaterialsList.Count >= 5)
                return false;

            return base.Add(book);
        }
    }
}
