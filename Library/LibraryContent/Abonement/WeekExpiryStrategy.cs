﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Client;

namespace Library.LibraryContent.Abonement
{
    public class WeekExpiryStrategy : IExpiryDateCalculationStrategy
    {
        public DateTime CalculateExpiryDate(LibraryMaterial material, DateTime @from)
        {
            return from.AddDays(7);
        }
    }
}
