﻿using System;

namespace Library.LibraryContent.Abonement
{
    public class MaterialWithExpirationDate
    {
        public LibraryMaterial Material { get; }
        public DateTime Expiry { get; }

        public MaterialWithExpirationDate(LibraryMaterial material, DateTime time)
        {
            Expiry = time;
            Material = material;
        }
    }
}
