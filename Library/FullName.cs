﻿using System;

namespace Library
{
    public class FullName
    {
        public FullName(string firstName, string middleName, string lastName)
        {
            FirstName = firstName ?? throw new ArgumentException(nameof(firstName));
            MiddleName = middleName ?? throw new ArgumentException(nameof(middleName));
            LastName = lastName ?? throw new ArgumentException(nameof(lastName));
        }

        public string FirstName { get; }
        public string MiddleName { get; }
        public string LastName { get; }
    }
}
