﻿using System;
using Library.Client;

namespace Library.Client
{
    public class LibraryClient
    {
        public LibraryClient(FullName name, Faculty faculty, DateTime registered, Contacts contacts, ClientType type)
        {
            if(!Enum.IsDefined(typeof(Faculty), faculty))
                throw new ArgumentException(nameof(faculty));
            if(!Enum.IsDefined(typeof(ClientType), type))
                throw new  ArgumentException(nameof(type));
            Name = name;
            Faculty = faculty;
            Registered = registered;
            Type = type;
            Contacts = contacts ?? throw new ArgumentException(nameof(contacts));
        }

        public FullName Name { get; }
        public Faculty Faculty { get; }
        public DateTime Registered { get; }
        public Contacts Contacts { get; }
        public ClientType Type { get; }
    }
}
