﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library.Client
{
    public class Contacts
    {
        public string Email { get; }
        public string PhoneNumber { get; }
        public string Address { get; }

        public Contacts(string email, string phoneNumber, string address)
        {
            if(!IsValidEmail(email))
                throw new ArgumentException("Invalid email address", nameof(email));
            if(!IsValidPhoneNumber(phoneNumber))
                throw new ArgumentException("Invalid phone number", nameof(phoneNumber));
            Email = email 
                ?? throw new ArgumentException(nameof(email));
            PhoneNumber = phoneNumber 
                ?? throw new ArgumentException(nameof(phoneNumber));
            Address = address 
                ?? throw new ArgumentException(nameof(address));
        }

        private bool IsValidPhoneNumber(string number)
        {
            return Regex.Match(number, @"^(\+[0-9]{11,12})$").Success;
        }


        private bool IsValidEmail(string address)
        {
            try
            {
                var mailAddress = new MailAddress(address);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}
