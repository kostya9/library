﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Library.Client;
using Library.LibraryContent;
using Library.LibraryContent.Abonement;

namespace Library
{
    public class Library
    {
        private readonly Func<IExpiryDateCalculationStrategy> _expiryDateCalculationStrategyFactory;
        private readonly Dictionary<LibraryClient, Abonement> _abonements;
        private readonly IEnumerable<LibraryMaterial> _electronicMaterials;
        private readonly IEnumerable<LibraryMaterial> _physicalMaterials;
        public IEnumerable<MaterialCover> ElectronicMaterials => _electronicMaterials.Select(m => new MaterialCover(m));
        public IEnumerable<MaterialCover> PhysicalMaterials => _physicalMaterials.Select(m => new MaterialCover(m));

        public Library(IEnumerable<LibraryMaterial> materials, Func<IExpiryDateCalculationStrategy> expiryDateCalculationStrategyFactory)
        {
            _expiryDateCalculationStrategyFactory = expiryDateCalculationStrategyFactory;
            if (materials == null)
                throw new ArgumentException(nameof(materials));

            var enumerated = materials.ToList();

            _electronicMaterials = enumerated.Where(i => i.Type == MaterialType.Electronic);
            _physicalMaterials = enumerated.Where(i => i.Type == MaterialType.Physical);
            _abonements = new Dictionary<LibraryClient, Abonement>();
        }

        private Abonement GetAbonement(LibraryClient client)
        {
            if (!_abonements.ContainsKey(client))
                _abonements[client] = client.Type == ClientType.Teacher
                    ? new Abonement(_expiryDateCalculationStrategyFactory())
                    : new StudentAbonement(_expiryDateCalculationStrategyFactory());

            return _abonements[client];
        }

        public IMaterialContainer GetMaterialsFromAbonement(LibraryClient client) => GetAbonement(client);

        public bool IsInUse(MaterialCover cover)
        {
            Validate(cover);
            return _abonements.Any(a => a.Value.Materials.Any(b => b.Material == cover.Material));
        }

        public void ReturnPhysicalMaterial(LibraryClient client, LibraryMaterial material)
        {
            if(material.Type != MaterialType.Physical)
                throw new ArgumentException($"The material is not physical");

            var abonement = GetAbonement(client);
            abonement.Remove(material);
        }

        private bool Exists(MaterialCover cover) => _electronicMaterials.Any(b => b == cover.Material) || _physicalMaterials.Any(b => b == cover.Material);

        public bool TryGiveOutPhysicalMaterial(LibraryClient client, MaterialCover cover)
        {
            Validate(cover);

            if(client == null)
                throw new ArgumentException(nameof(client));

            var libraryMaterial = cover.Material;
            if(libraryMaterial.Type != MaterialType.Physical)
                throw new ArgumentException("The material is not physical");

            if (IsInUse(cover))
                return false;

            var abonement = GetAbonement(client);
            var success = abonement.Add(libraryMaterial);
            if (success)
                libraryMaterial.Statistics.UpdateUsages(client);
            return success;
        }

        public LibraryMaterial GetElectronicMaterial(LibraryClient client, MaterialCover cover)
        {
            Validate(cover);

            if(client == null)
                throw new ArgumentException(nameof(client));

            var libraryMaterial = cover.Material;
            if (libraryMaterial.Type != MaterialType.Electronic)
                throw new ArgumentException($"The material is not electronic");

            libraryMaterial.Statistics.UpdateUsages(client);
            return libraryMaterial;
        }

        private void Validate(MaterialCover cover)
        {
            if (cover == null)
                throw new ArgumentException(nameof(cover));

            if (!Exists(cover))
                throw new ArgumentException($"This material does not exist in the library");
        }
    }
}
