﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Client;
using Library.LibraryContent;
using Library;
using Library.LibraryContent.Abonement;

namespace LibraryClient
{
    class Program
    {
        static void Main(string[] args)
        {
            FullName fullName = new FullName("123", "229", "098");
            string name = "Alice";
            string content = name;
            var libraryMaterials = new LibraryMaterial[]
            {
                new LibraryMaterial(name, fullName, DateTime.Now, DateTime.Now, MaterialType.Electronic, content),
                new LibraryMaterial("Dave in the", fullName, DateTime.Now, DateTime.Now, MaterialType.Physical, content),
                new LibraryMaterial("Lol in the", fullName, DateTime.Now, DateTime.Now, MaterialType.Electronic, content),
                new LibraryMaterial(name, fullName, DateTime.Now, DateTime.Now, MaterialType.Physical, content),
                new LibraryMaterial(name, fullName, DateTime.Now, DateTime.Now, MaterialType.Physical, content)
            };
            var library = new Library.Library(libraryMaterials, () => new WeekExpiryStrategy());

            var client = new Library.Client.LibraryClient(fullName, Faculty.FPM, DateTime.Now, new Contacts("kostya9@gmail.com", "+380990990909", "Some str"), ClientType.Student);
            var book = library.PhysicalMaterials.First(i => i.Name.StartsWith("Dave"));
            if (!library.TryGiveOutPhysicalMaterial(client, book))
                Console.WriteLine("Sorry, we have no books like that");
            var materials = library.GetMaterialsFromAbonement(client);
            Console.WriteLine(materials.Materials.First().Material.Name);
            Console.WriteLine(materials.Materials.First().Material.Statistics.Usages);
            Console.WriteLine(materials.Materials.First().Material.Statistics.StudentUsages);
            
        }
    }
}
